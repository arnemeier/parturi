package com.valiit.parturi.repository;

import com.valiit.parturi.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CustomerRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Customer getCustomer(String email) {
        List<Customer> customers = jdbcTemplate.query("select * from customer where email = ?", new Object[]{email}, mapCustomerRows);
        return customers.size() > 0 ? customers.get(0) : null;
    }

    public boolean endUserExists(String username) {
        Integer count = jdbcTemplate.queryForObject(
                "select count(id) from customer where email = ?",
                new Object[]{username},
                Integer.class
        );
        return count != null && count > 0;
    }

    String sqlString= "\"insert into `customer` (`firstName`, `lastName`, `email`, `password`, `phone`)\" +\n" +
            "                \"VALUES (?, ?, ?, ?, ?)\"";



    public void addCustomer(Customer customer) {
        jdbcTemplate.update("insert into `customer` (`firstName`, `lastName`, `email`, `password`, `phone`)" +
                "VALUES (?, ?, ?, ?, ?)", customer.getFirstName(), customer.getLastName(), customer.getEmail(), customer.getPassword(), customer.getPhone());
    }

    public void updateCustomer(Customer customer) {
        jdbcTemplate.update("UPDATE customer SET firstName = ?," +
                        "lastName = ?, email = ?, password = ?," +
                        "phone = ? WHERE id = ?",
                customer.getFirstName(),
                customer.getLastName(),
                customer.getEmail(),
                customer.getPassword(),
                customer.getPhone(),
                customer.getId());
    }

    public Customer getCustomerByUsername(String username) {
        List<Customer> customers = jdbcTemplate.query(
                "select * from `customer` where `email` = ?",
                new Object[]{username},
                (rs, rowNum) -> new Customer(rs.getInt("id"),
                        rs.getString("firstName"),
                        rs.getString("lastName"),
                        rs.getString("email"),
                        rs.getString("password"),
                        rs.getString("phone"))
        );
        return customers.size() > 0 ? customers.get(0) : null;
    }

    //    public List<Customer> getCustomers() {
//        return jdbcTemplate.query("select * from customer", mapCustomerRows);
//    }
//
    private RowMapper<Customer> mapCustomerRows = (rs, rowNum) -> {
        Customer customer = new Customer(
                rs.getInt("id"),
                rs.getString("firstName"),
                rs.getString("lastName"),
                rs.getString("email"),
                rs.getString("password"),
                rs.getString("phone"));
        return customer;
    };


}
