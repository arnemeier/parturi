package com.valiit.parturi.rest;

import com.valiit.parturi.dto.CustomerDto;
import com.valiit.parturi.dto.GenericResponseDto;
import com.valiit.parturi.dto.JwtRequestDto;
import com.valiit.parturi.dto.JwtResponseDto;
import com.valiit.parturi.model.Customer;
import com.valiit.parturi.repository.CustomerRepository;
import com.valiit.parturi.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@CrossOrigin("*")
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @Autowired
    private CustomerRepository customerRepository;

//    @GetMapping
//    public List<Customer> getCustomers(){
//        return customerRepository.getCustomers();
//    }

    @PostMapping("/login")
    public JwtResponseDto authenticate(@RequestBody JwtRequestDto request) throws Exception {
        return customerService.authenticate(request);
    }

    @PostMapping("/register")
    public GenericResponseDto register(@RequestBody CustomerDto customerRegistration) {
        return customerService.register(customerRegistration);
    }

    @GetMapping("/{email}")
    public CustomerDto getCustomer(@PathVariable("email") String email) {
        return customerService.getCustomer(email);
    }

    @PostMapping("/update")
    public String updateCustomer(@RequestBody CustomerDto customerUpdate){
        return customerService.update(customerUpdate);
    }

}
