package com.valiit.parturi.service;

import com.valiit.parturi.dto.CustomerDto;
import com.valiit.parturi.dto.GenericResponseDto;
import com.valiit.parturi.dto.JwtRequestDto;
import com.valiit.parturi.dto.JwtResponseDto;
import com.valiit.parturi.model.Customer;
import com.valiit.parturi.repository.CustomerRepository;
import com.valiit.parturi.util.Transformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenService jwtTokenService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public GenericResponseDto register(CustomerDto customerRegistration) {
        GenericResponseDto responseDto = new GenericResponseDto();
        Customer customer = new Customer(
                customerRegistration.getFirstName(),
                customerRegistration.getLastName(),
                customerRegistration.getEmail(),
                passwordEncoder.encode(customerRegistration.getPassword()),
                customerRegistration.getPhone());
        if (!customerRepository.endUserExists(customerRegistration.getEmail())) {
            customerRepository.addCustomer(customer);
        } else {
            responseDto.getErrors().add("User with the specified username already exists.");
        }
        return responseDto;
    }

    public JwtResponseDto authenticate(JwtRequestDto request) throws Exception {
        authenticate(request.getEmail(), request.getPassword());
        final Customer customerDetails = customerRepository.getCustomerByUsername(request.getEmail());
        final String token = jwtTokenService.generateToken(customerDetails.getEmail());
        return new JwtResponseDto(customerDetails.getId(), customerDetails.getEmail(), token);
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

    public CustomerDto getCustomer(String email) {
        Assert.isTrue(email != "", "The ID of the user not specified");

        Customer customer = customerRepository.getCustomer(email);
        return Transformer.toCustomerDto(customer);
    }

    public String update(CustomerDto customerUpdate) {
        Customer currentCustomer = getCurrentCustomer();

        if (
                customerUpdate.getEmail().equals(currentCustomer.getEmail()) ||
                        customerRepository.getCustomerByUsername(customerUpdate.getEmail()) == null) {
            //happy
            currentCustomer.setFirstName(customerUpdate.getFirstName());
            currentCustomer.setLastName(customerUpdate.getLastName());
            currentCustomer.setEmail(customerUpdate.getEmail());
            currentCustomer.setPassword(passwordEncoder.encode(customerUpdate.getPassword()));
            currentCustomer.setPhone(customerUpdate.getPhone());

            customerRepository.updateCustomer(currentCustomer);
        } else {
            //not-so-happy
            System.out.println("siin tegeleme probleemidega");
            return "";
        }


        return jwtTokenService.generateToken(currentCustomer.getEmail());

    }


    public Customer getCurrentCustomer() {
        String loggedInUsername = ((org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        return customerRepository.getCustomerByUsername(loggedInUsername);
    }


}
