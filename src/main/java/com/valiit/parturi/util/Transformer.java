package com.valiit.parturi.util;

import com.valiit.parturi.dto.CustomerDto;
import com.valiit.parturi.model.Customer;

public class Transformer {

//    public static Company toCompanyModel(CompanyDto initialObject) {
//        if (initialObject == null) {
//            return null;
//        }
//
//        Company resultingObject = new Company();
//        resultingObject.setId(initialObject.getId());
//        resultingObject.setName(initialObject.getName());
//        resultingObject.setLogo(initialObject.getLogo());
//        resultingObject.setEstablished(initialObject.getEstablished());
//        resultingObject.setEmployees(initialObject.getEmployees());
//        resultingObject.setRevenue(initialObject.getRevenue());
//        resultingObject.setNetIncome(initialObject.getNetIncome());
//        resultingObject.setSecurities(initialObject.getSecurities());
//        resultingObject.setSecurityPrice(initialObject.getSecurityPrice());
//        resultingObject.setDividends(initialObject.getDividends());
//        return resultingObject;
//    }

    public static CustomerDto toCustomerDto(Customer initialObject) {
        if (initialObject == null) {
            return null;
        }

        CustomerDto resultingObject = new CustomerDto();
        resultingObject.setId(initialObject.getId());
        resultingObject.setFirstName(initialObject.getFirstName());
        resultingObject.setLastName(initialObject.getLastName());
        resultingObject.setEmail(initialObject.getEmail());
        resultingObject.setPhone(initialObject.getPhone());

        return resultingObject;
    }

}
