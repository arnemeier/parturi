	INSERT INTO `customer` (`firstName`, `lastName`, `email`, `password`, `phone`) VALUES
    ('eesnimi', 'perenimi', 'a@b.cd', '$2a$10$dKFHrcGzqFziQ1clLEvp8.mV0APM92/xu/5O4gfsXCuIYwVzvRvKi', '12345-66'),
    ('juhan', 'julm', 'juhan@julm.ee', '$2a$10$60D5ze9kRk3//NIVBFQw6.Nu0tVrV5nek9b9NTwBcciu9GPZnJJHK', '66-12345-66'),
    ('kaarel', 'karm', 'kaarel@karm.ee', '$2a$10$DIWGStJCUTS47EpoLzo33O41nEmH2f5nCr0/Blbbyr.VgLRFTcCOO', '66-12345-66');
